<?php
/**
 * Template Name: Dashboard
 *
 * @package Listable
 * @since Listable 1.2.5
 */

get_header(); ?>
<style type="text/css">
	/*.page-header{ background: none;font-size: 25px;margin-top: 0 !important;
    width: 100% !important;}*/
    .page-header{display: none;}
</style>
<?php
global $current_user, $wpdb;
$role = $wpdb->prefix . 'capabilities';
$current_user->role = array_keys($current_user->$role);
$role = $current_user->role[0];
$c_id = $current_user->ID;
//echo $c_id;
?>
 <div>
 <?php $showword = explode('/', $_SERVER['REQUEST_URI']);
//echo $showword[2];
?>

 		<?php if($role != 'employer'){?>
       <nav>
            <div class="navbar-default sidebar deshboards" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="<?php if($showword[2] == 'dashboard'){ echo 'active';}?>">
                            <a href="<?php echo site_url();?>/dashboard"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li class="<?php if($showword[2] == 'login'){ echo 'active';}?>">
                            <a href="<?php echo get_permalink(11309);?>"><i class="fa fa-cog" aria-hidden="true"></i> Account Setting</a>
                        </li>
                 
                        <!--<li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> Number of successful cuts.</a>
                        </li>-->
                        <li class="<?php if($showword[2] == 'add-barber-profile' || $showword[2] == 'edit-barbers-profile'){ echo 'active';}?>">
                        <?php 
//echo  $c_id;
                       $profile_id = get_barber_profile_id($c_id);
                       //echo $profile_id;
                       if($profile_id != 0){?>
                            <a href="<?php echo get_permalink(11208);?>?action=edit&amp;job_id=<?php echo $profile_id;?>"><i class="fa fa-sitemap fa-fw"></i> Manage Profile </a>
                        <?php }else{?>                            
                            <a href="<?php echo get_permalink(11248);?>"><i class="fa fa-sitemap fa-fw"></i> Add profile</a>
                        <?php }?>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>

        <div id="page-wrapper" class="dashboards">
            <div class="row">
        <?php }?>
			<?php $page_id = get_the_ID();if($role != 'employer' && $page_id=='11203'){?>
<div class="col-lg-3 col-md-6 m15">
                    <div class="panel panel-primary">

                        <div class="panel-heading">

                            <div class="row">

                               <div class="all-rivews"><p> Manage Profile!</p></div>

                                <div class="col-xs-3">

                                    <i class="fa fa-comments fa-5x"></i>

                                </div>

                                <div class="col-xs-9">

                                   <!-- <div class="huge">26</div>-->

                                </div>

                            </div>

                        </div>
			<?php  $profile_id = get_barber_profile_id($c_id);
                       //echo $profile_id;
                       if($profile_id != 0){?>
                            <a href="<?php echo get_permalink(11208);?>?action=edit&amp;job_id=<?php echo $profile_id;?>">
                        <?php }else{?>
                            <a href="<?php echo get_permalink(11248);?>">
                        <?php }?>

                            <div class="panel-footer">

                                <span class="pull-left">View Details</span>

                                <span class=""><i class="fa fa-arrow-circle-right"></i></span>

                                <div class="clearfix"></div>

                            </div>

                        </a>

                    </div>

                </div>



                <div class="col-lg-3 col-md-6 m15">

                    <div class="panel panel-red">

                        <div class="panel-heading">

                            <div class="row">

                             <div class="all-rivews"><p>Account Setting!</p></div>

                                <div class="col-xs-3">

                                    <i class="fa fa-support fa-5x"></i>

                                </div>

                                <div class="col-xs-9">

                                   <!-- <div class="huge">13</div>-->

                                </div>

                            </div>

                        </div>

                        <a href="<?php echo get_permalink(11309);?>">

                            <div class="panel-footer">

                                <span class="pull-left">View Details</span>

                                <span class=""><i class="fa fa-arrow-circle-right"></i></span>

                                <div class="clearfix"></div>

                            </div>

                        </a>

                    </div>

                </div>
            <?php }?>

	 <?php while ( have_posts() ) : the_post();
                get_template_part( 'template-parts/content', 'page' );
            endwhile; // End of the loop. ?>
            
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->
    <?php 
	wp_enqueue_style( 'bootstrap.min', get_stylesheet_directory_uri() . '/dashboard/bootstrap.min.css' );
	wp_enqueue_style( 'sb-admin-2', get_stylesheet_directory_uri() . '/dashboard/sb-admin-2.css' );
    ?>

<?php
get_sidebar();
get_footer();
 ?>


 <?php $member_id = SwpmMemberUtils::get_logged_in_members_id();
      $member_level = SwpmMemberUtils::get_member_field_by_id($member_id, 'membership_level');
      //echo $member_level;
      if($member_level == 4)
        {?>
        <style>
.rpt_plan.rpt_plan_1::before {position: absolute;content: '';background: rgba(251,251,251,0.5);width: 100% !important;z-index: 99;height: 100% !important;
}
.rpt_plan.rpt_plan_1::after {position: absolute;content: 'subscribed';width: 157px;height: 37px; right: -41px; top: 17px;color: #4a4949;
    background: #ccc;text-align: center;transform: rotate(45deg);font-size: 13px;line-height: 36px;z-index: 99999;text-transform: uppercase;
}
            </style>
        <?php }else if($member_level == 5){?>
            <style>
.rpt_plan.rpt_plan_1::before {position: absolute; content: '';background: rgba(251,251,251,0.5);width: 100% !important;z-index: 99; height: 100% !important;
}
.rpt_plan.rpt_plan_1::after {position: absolute;content: 'subscribed'; width: 157px;height: 37px;right: -41px;top: 17px;color: #4a4949;background: #ccc;
    text-align: center;transform: rotate(45deg);font-size: 13px;line-height: 36px;z-index: 99999;text-transform: uppercase;}
.rpt_3_plans .rpt_plan_2{position: relative; overflow: hidden;}
.rpt_plan.rpt_plan_2::before {position: absolute; content: ''; background: rgba(251,251,251,0.5); width: 100% !important; z-index: 99; height: 100% !important;
}
.rpt_plan.rpt_plan_2::after { position: absolute; content: 'subscribed'; width: 157px; height: 37px; right: -41px;top: 17px; color: #4a4949;
    background: #ccc;text-align: center;transform: rotate(45deg);font-size: 13px;line-height: 36px;z-index: 99999;text-transform: uppercase;
}
            </style>
        <?php }?>
