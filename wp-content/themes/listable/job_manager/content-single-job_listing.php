<?php
/**
 * The template for displaying the WP Job Manager listing details on single listing pages
 *
 * @package Listable
 */

global $post;

$taxonomies = array();
$data_output = '';
$terms = get_the_terms(get_the_ID(), 'job_listing_type');
$termString = '';
if ( ! is_wp_error( $terms ) && ( is_array( $terms ) || is_object( $terms ) ) ) {
	$firstTerm = $terms[0];
	if ( ! $firstTerm == NULL ) {
		$term_id = $firstTerm->term_id;

		$data_output .= 'data-icon="' . listable_get_term_icon_url($term_id) .'"';
		$count = 1;
		foreach ( $terms as $term ) {
			$termString .= $term->name;
			if ( $count != count($terms) ) {
				$termString .= ', ';
			}
			$count++;
		}
	}
}

$listing_is_claimed = false;
if ( class_exists( 'WP_Job_Manager_Claim_Listing' ) ) {
	$classes = WP_Job_Manager_Claim_Listing()->listing->add_post_class( array(), '', $post->ID );

	if ( isset( $classes[0] ) && ! empty( $classes[0] ) ) {
		if ( $classes[0] == 'claimed' )
			$listing_is_claimed = true;
	}
} ?>

<div class="single_job_listing"
	data-latitude="<?php echo get_post_meta($post->ID, 'geolocation_lat', true); ?>"
	data-longitude="<?php echo get_post_meta($post->ID, 'geolocation_long', true); ?>"
	data-categories="<?php echo $termString; ?>"
	<?php echo $data_output; ?>>

	<?php if ( get_option( 'job_manager_hide_expired_content', 1 ) && 'expired' === $post->post_status ) : ?>
		<div class="job-manager-info"><?php esc_html_e( 'This listing has expired.', 'listable' ); ?></div>
	<?php else : ?>
		<div class="grid">
			<div class="grid__item  column-content  entry-content">
				<header class="entry-header">
        	<div class="inner-header">
        		<div class="barber-profile-div">
				<?php $author_id=$post->post_author;
				      $user_info = get_userdata($author_id);
				      $profile_img = get_user_meta( $author_id, 'avtar_image', 'true' );

				      $booking_code = get_user_meta( $author_id, 'booking_code', 'true' );
				      
				      $member_id = SwpmMemberUtils::get_logged_in_members_id();
				      $profile_phone = SwpmMemberUtils::get_member_field_by_id($member_id, 'phone');
				      //$profile_phone = get_user_meta($author_id,'phone','true')	;
				      
					?>
        			<div class="left">
				<?php if(!empty($profile_img)){?>
        			<img width="200px" height="200px" alt="barbertest&lt;br/&gt;" src="<?php echo $profile_img;?>">
        			<?php }else{?>
        			<img width="200px" height="200px" src="<?php echo site_url();?>/wp-content/uploads/2016/08/profile.png">
        			<?php }?>
        			</div>
        			<div class="right">
        				<div itemprop="name" class="barber-title"><?php
						echo get_the_title();
						if ( $listing_is_claimed ) :
							echo '<span class="listing-claimed-icon">';
							get_template_part('assets/svg/checked-icon');
							echo '<span>';
						endif;
					?>
					<span>Owner &amp; Barber</span></div>
        				<div class="star"><?php 
        				do_action( 'single_job_listing_start' );?></div>
        				<?php if(!empty($profile_phone)){?><a class="phone listing-contact  listing--phone"><?php echo $profile_phone;?></a><?php }?>
        				<a href="mailto:<?php echo $user_info->user_email;?>" class="email listing-contact  listing--email"><?php echo $user_info->user_email;?></a>
        			</div>
                </div>
        	</div>
                    </header>
				<?php if ( is_active_sidebar( 'listing_content' ) ) : ?>
					<div class="listing-sidebar  listing-sidebar--main">
						<?php dynamic_sidebar('listing_content'); ?>
					</div>
				<?php endif; ?>
			</div> <!-- / .column-1 -->

			<div class="grid__item  column-sidebar">
				<?php if ( is_active_sidebar( 'listing__sticky_sidebar' ) ) : ?>
					<div class="listing-sidebar  listing-sidebar--top  listing-sidebar--secondary">
						<?php dynamic_sidebar('listing__sticky_sidebar'); ?>
					</div>
				<?php endif; ?>

				<?php if(!empty($booking_code)){?>
				<div class="listing-sidebar  listing-sidebar--top  listing-sidebar--secondary">
						<?php echo $booking_code; ?>
					</div>
				<?php }?>
  
				<?php if ( is_active_sidebar( 'listing_sidebar' ) ) : ?>
					<div class="listing-sidebar  listing-sidebar--bottom  listing-sidebar--secondary">
						<?php dynamic_sidebar('listing_sidebar'); ?>sdf
					</div>
				<?php endif; ?>

			</div><!-- / .column-2 -->
		</div>
	<?php endif; ?>
</div>


