<?php
    // check if accessed directly
    if ( ! defined( 'ABSPATH' ) ) exit;
?>
<div class="wbk-outer-container">
	<div class="wbk-inner-container">
 	<img src=<?php echo get_site_url() . '/wp-content/plugins/webba-booking/frontend/images/loading.svg' ?> style="display:block;width:0px;height:0px;">
		<div class="wbk-frontend-row" >
			<div class="wbk-col-12-12" >		
				 <?php 			
					/*for barber id*/
					 global $post;
		        		$barber_id=$post->post_author;
				 	if ( $data[0] <> 0 ){
				 		echo '<input type="hidden" id="wbk-service-id" value="' . $data[0] . '" />';	 	 		
				 	} else {

				 		$label = '';	
				 		if ( get_option( 'wbk_mode', 'extended' ) == 'extended' ) {
					 		$label = get_option( 'wbk_service_label', 'Select service' );
				 		
				 		} else {
					 		$label = get_option( 'wbk_service_label', 'Select service' );
				 		}
						//echo  '<label class="wbk-input-label">' . $label . '</label>';
				
				 		echo '<select class="wbk-select wbk-input" id="wbk-service-id" style="display:none;">'; 
				 		//echo '<option value="0" selected="selected">' . __( 'select...', 'wbk' ) . '</option>';
				 		$arrIds = WBK_Db_Utils::getServices();
				 		foreach ( $arrIds as $id ) {
				 			 
				 			$service = new WBK_Service();
				 			if ( !$service->setId( $id ) ) {  
				 				continue;
				 			}
				 			if ( !$service->load() ) {  
				 				 
				 				continue;
				 			}
				 			echo '<option value="' . $service->getId() . '"" >' . $service->getName() . '</option>';
				 		}
				 		echo '</select>';
						echo '<div class="barber-title">Barber booking</div>';
				 		echo '<input type="hidden" class="wbk-barberid wbk-input" name="wbk-barberid"  id="wbk-barberid" value="'.$barber_id.'">'; 
				 	}
				 ?>
			</div>
			

		</div>
		<div class="wbk-frontend-row" id="wbk-date-container">			 
		</div> 
		<div class="wbk-frontend-row" id="wbk-time-container">
		</div>
		<div class="wbk-frontend-row" id="wbk-slots-container">				 
		</div>
		<div class="wbk-frontend-row" id="wbk-booking-form-container">		 
		</div>
		<div class="wbk-frontend-row" id="wbk-booking-done">
		</div>
	</div>	
</div>
