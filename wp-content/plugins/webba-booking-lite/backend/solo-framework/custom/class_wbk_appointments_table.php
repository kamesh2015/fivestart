<?php
//WBK appointment table class

// check if accessed directly
add_action( 'wp_ajax_wbk_get_free_time_for_day',  'wbkGetFreeTimeForDay' ); 
function wbkGetFreeTimeForDay(){
    $day = sanitize_text_field( $_POST['day'] );
    $appointment_id = sanitize_text_field( $_POST['row_id'] );
    if( !is_numeric( $appointment_id ) ){
        echo 'Incorrect appointment id';
        die();
        return;
    }
    $service_id = WBK_Db_Utils::getServiceIdByAppointmentId( $appointment_id );
    $service_schedule = new WBK_Service_Schedule();
    if ( !$service_schedule->setServiceId( $service_id ) ){
        echo 'Unable to create service schedule';        
        die();
        return;
    }
    if ( !$service_schedule->load() ){
        echo 'Unable to load schedule'; 
        die();
        return;
    }
    $time = strtotime( $day );
    if ( $time == false ){
        echo 'Unable to convert date'; 
        die();
        return;
    }   
    $midnight = strtotime( 'today', $time );
    $day_status =  $service_schedule->getDayStatus( $midnight );
    if ( $day_status == 0 ) {
        echo '<option value="-1">'. __( 'Time slots not found', 'wbk' ) .'</option>';
        die();
        return;
    }
    $service_schedule->buildSchedule( $midnight );
    $result = $service_schedule->renderSelectOptionsFreeTimslot( $appointment_id );
    echo $result;
    die();
    return;
}
 



if ( ! defined( 'ABSPATH' ) ) exit;
class WBK_Appointments_Table extends SLFTable {
	 

	public function __construct() {
			
            
			$this->field_set = new SLFFieldSet();

            $field = new SLFField( array( 'title' => __( 'Service','wbk' ),     
                                         'name' => 'service_id',
                                         'format' => '%d',
                                         'component' => 'SLFTableWbkService',
                                          
                                         'render_cell' => true,
                                         'render_control' => false,
                                          'updatable' => false
                                          )

                                 );
            $this->field_set->append( $field );


            $field = new SLFField( array( 'title' => __( 'Date','wbk' ),     
                                         'name' => 'day',
                                         'format' => '%d',
                                         'component' => 'SLFTableDate',
                                          
                                         'render_cell' => true,
                                         'render_control' => true,
                                         'updatable' => false
                                        )

                                 );
            $this->field_set->append( $field );

            $field = new SLFField( array( 'title' => __( 'Time','wbk' ),     
                                         'name' => 'time',
                                         'format' => '%d',
                                         'component' => 'SLFTableSelect',
                                         
                                        
                                         'render_cell' => true,
                                         'render_control' => true,
                                         'updatable' => true
                                          )

                                 );
            $this->field_set->append( $field );

            $field = new SLFField( array( 'title' => __( 'Customer name','wbk' ),     
                                         'name' => 'name',
                                         'format' => '%s',
                                         'component' => 'SLFTableText',
                                         
                                         'render_cell' => true,
                                         'render_control' => true,
                                         'updatable' => true
                                          )

                                 );
            $this->field_set->append( $field );

            $field = new SLFField( array('title' => __( 'Customer email', 'wbk' ),     
                                         'name' => 'email',
                                         'format' => '%s',
                                         'component' => 'SLFTableText',
                                          
                                         'render_cell' => true,
                                         'render_control' => true,
                                         'updatable' => true

                                          )
                                 );
            $this->field_set->append( $field );

            $field = new SLFField( array('title' => __( 'Customer phone', 'wbk' ),     
                                         'name' => 'phone',
                                         'format' => '%s',
                                         'component' => 'SLFTableText',
                                        
                                           
                                         'render_cell' => true,
                                         'render_control' => true,
                                         'updatable' => true

                                          )
                                 );
            $this->field_set->append( $field );
            $field = new SLFField( array('title' => __( 'Places booked', 'wbk' ),     
                                         'name' => 'quantity',
                                         'format' => '%d',
                                         'component' => 'SLFTableText',
                                         'assoc' => 'time',
                                         'render_cell' => true,
                                         'render_control' => true,
                                         'updatable' => true

                                          )
                                 );
            $this->field_set->append( $field );

            $field = new SLFField( array('title' => __( 'Customer comment', 'wbk' ),     
                                         'name' => 'description',
                                         'format' => '%s',
                                         'component' => 'SLFTableText',
       
                                          'render_cell' => true,
                                         'render_control' => true,
                                         'updatable' => true

                                          )
                                 );
            $this->field_set->append( $field );
         


            $this->table_name = 'wbk_appointments';

            $this->filter_set = array();

            $filter = new SLFTableFilterDateRange( __( 'Select date range:', 'wbk' ), 'day' );
            $filter->setDefault();
            $this->filter_set['day'] = $filter;

            $filter = new WBKTableFilterServices( __( 'Select services:', 'wbk' ), 'service_id' );
            $filter->setDefault();
            $this->filter_set['service_id'] = $filter;
            
	}

}