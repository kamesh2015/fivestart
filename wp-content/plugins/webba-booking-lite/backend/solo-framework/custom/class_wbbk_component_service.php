<?php
// Solo Framework table text component
if ( ! defined( 'ABSPATH' ) ) exit;
class SLFTableWbkService extends SLFTableComponent {
	public function __construct( $title, $name, $value ) {
		parent::__construct( $title, $name, $value );
	}
    public function renderCell(){
		$error_message = 'Internal error: unable to init service.';
		if ( !is_numeric( $this->value ) ){
			return $error_message;
		}
		$service = new WBK_Service();
		if ( !$service->setId( $this->value  ) ) {
			return $error_message;
		}
		if ( !$service->load() ) {
 			return $error_message;
		}
		return $service->getName();    	   	
    }
    public function renderControl(){
    	$html = '';	 
		return $html;
    }
}
