<?php
// Solo Framework table text component
if ( ! defined( 'ABSPATH' ) ) exit;

class SLFTableSelect extends SLFTableComponent {


	public function __construct( $title, $name, $value, $min, $max ) {
		parent::__construct( $title, $name, $value, $min, $max );
	}
	
    public function renderCell(){
    	if ( $this->name == 'time'){
				$format = get_option( 'time_format' );
				return date_i18n( $format,   $this->value );  	
    	}
		return $this->value;    	
    }
    public function renderControl(){
    	$html = '<label class="slf_table_component_label" >' . $this->title . '</label>';
		$html .= '<select class="slf_table_component_select slf_table_component_input" name="' . $this->name . '" data-type="select"  />';


		$html .= '</select>';
		return $html;
    }


}
