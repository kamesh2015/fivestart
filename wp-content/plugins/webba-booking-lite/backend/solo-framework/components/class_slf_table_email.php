<?php
// Solo Framework table text component
if ( ! defined( 'ABSPATH' ) ) exit;

class SLFTableEmail extends SLFTableComponent {


	public function __construct( $title, $name, $value, $min, $max ) {
		parent::__construct( $title, $name, $value, $min, $max );
	}
	
    public function renderCell(){
		return $this->value;    	

    }
    public function renderControl(){
    	$html = '<label class="slf_table_component_label" >' . $this->title . '</label>';
		$html .= '<input class="slf_table_component_input" name="' . $this->name . '" data-type="email"  type="text" value="' . $this->value . '"  />';
		return $html;
    }


}
