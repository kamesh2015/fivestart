<?php
// Solo Framework table text component
if ( ! defined( 'ABSPATH' ) ) exit;

class SLFTableDate extends SLFTableComponent {

	public function __construct( $title, $name, $value, $min, $max ) {
		parent::__construct( $title, $name, $value, $min, $max );
	}
	
    public function renderCell(){

    	$format = get_option( 'date_format' );
		return date_i18n( $format,   $this->value );  	

    }
    public function renderControl(){
    	$format = get_option( 'date_format' );
    	$html = '<label class="slf_table_component_label" >' . $this->title . '</label>';
		$html .= '<input class="slf_table_component_input slf_table_component_date" name="' . $this->name . '" data-type="date"  type="text" value="' . date_i18n( $format,   $this->value ) . '"  />';
		return $html;
    }


}
