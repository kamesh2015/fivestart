<?php
// Solo Framework table class
if ( ! defined( 'ABSPATH' ) ) exit;
add_action( 'wp_ajax_slf_table_update',  'slfTableUpdate' );
// add_action( 'wp_ajax_slf_table_prepare_row',  'slfPrepareRow' ); 
// add_action( 'wp_ajax_slf_table_update_row',  'slfUpdateRow' ); 
function getAllowedClassNames() {
	return array( 'WBK_Appointments_Table' );
}
function slfPrepareRow(){
	$class_name = sanitize_text_field( $_POST['class_name'] );
	$row_id = sanitize_text_field ( $_POST['row_id'] );
	if ( !in_array( $class_name, getAllowedClassNames() ) ){
		echo 'ERROR: Undefined table.';
		die();
		return;
	} 
	$table = new $class_name();
	$result = $table->prepareRow( $row_id );
	echo $result;
	die();
	return;
}
function slfUpdateRow(){
	$class_name = sanitize_text_field( $_POST['class_name'] );
	$row_id = sanitize_text_field ( $_POST['row_id'] );
	$fields = $_POST['fields'];
	if ( !in_array( $class_name, getAllowedClassNames() ) ){
		echo 'ERROR: Undefined table.';
		die();
		return;
	}
	$fields_assoc = array();
	foreach ( $fields as $field ) {
		$fields_assoc[ $field[ 'name' ] ] = $field[ 'value' ];
	}
 	$table = new $class_name();
 	//var_dump($table->field_set);
	foreach ( $fields_assoc as $field_name => $value ) {
		$field = $table->field_set->getByName( $field_name );
		echo $field_name . '[' .$field->component . ']';
	}	
	die();
	return;
}
function slfTableUpdate(){
	$class_name = sanitize_text_field( $_POST['class_name'] );
	$filters = $_POST['filters'];
	if ( !in_array( $class_name, getAllowedClassNames() ) ){
		echo 'ERROR: Undefined table.';
		die();
		return;
	} 
	$table = new $class_name();
	foreach ( $filters as $filter ) {
		$filter_name = $filter['field'];
		$filrer_value = $filter['value'];
		if ( !$table->filter_set[$filter_name]->set($filrer_value) ){
			$table->filter_set[$filter_name]->setDefault();
		}
	}
  	$html = $table->renderTable();
 	echo $html;
 	die();
	return;
}
class SLFTable extends stdClass {
	public function __construct( $param = array() ) {
	}
	public function render(){
		$this->renderFilters();
		$this->renderData();
	}
	public function renderFilters(){
		$html = '<div class="slf_row slf_overflow_visible">';
		foreach ( $this->filter_set as $filter ) {
			$html .=  '<div class="slf_col_12_6_4">';
			$html .=  $filter->render();
			$html .=  '</div>';
		}
		$html .= '<div style="clear:both;"></div>';
		$html .= '</div>';
		echo $html;
	}
	public function renderData(){
		$html = '<input type="hidden" id="slf_table_class_name" value ="'. get_class($this) .'">';	
		$html .= '<div class="slf_row">';
		$html .= '<div id="slf-table-container">';
		$html .= $this->renderTable();
		$html .= '</div>';
		$html .= '</div>';
		echo $html;
	}
	public function renderTable(){
		global $wpdb;
		$col_names = array();
		$html = '<div class="slf_overlay"></div>';	
		$html .= '<table data-tablesaw-sortable class="tablesaw tablesaw-stack" data-tablesaw-mode="stack">';
		$html .=	'<thead>';	 
		$html .=		'<tr><th data-tablesaw-sortable-col data-tablesaw-sortable-default-col>' . __( 'ID', 'wbl' ) . '</th>';
		foreach ( $this->field_set->fields as $field ) { 
			if ( !$field->render_cell ){
				continue;
			}
			$html .=  '<th id="' . $field->name . '"  data-tablesaw-sortable-col>' . $field->title . '</th>';
			$col_names[] = $field->name;
		}
		$html .=		'</tr>';
		$html .=	'</thead>';
		$html .= '<tbody>';
		$sql = 'SELECT id,'. implode(',', $col_names ) . ' FROM ' . $this->table_name;
		$condition = '';
		foreach (	$this->filter_set as $filter ) {
			if ( $condition == '' ) {
				$condition .= ' WHERE ' . $filter->getSql();
			} else {
				$condition .= ' AND ' . $filter->getSql();
			}
		}
 		$sql .= $condition;
		$rows = $wpdb->get_results( 
			$sql, ARRAY_A 		
		);
		$row_num = 0;	
		foreach ( $rows as $row ) {
			$row_num++;
			$html .= '<tr id="slf-table-row-' . $row_num . '">';
				$i = 0;
				foreach ( $row  as  $key => $field ) {				 
					if ( $key == 'id' ){
					// $html_control = '<a class="slf-table-row-edit slf-table-icon" data-row-id="' . $row_num . '" data-app-id="' . $field . '" href="javascript:slf_table_prepare_row(' . $row_num . ', ' . $field . ', \'' . 
					// get_class($this).'\');"><span class="dashicons dashicons-edit"></span></a>';
						$html_control = '';
						$html .= '<td>' . $field . $html_control . '</td>';
					} else {
						if ( !$this->field_set->fields[ $key ]->render_cell ){
							continue;
						}
						$class_name =  $this->field_set->fields[ $key ]->component;
						$name = $this->field_set->fields[ $key ]->name;
						$value = $field;
						$min = $this->field_set->fields[ $key ]->valid_min;
						$max = $this->field_set->fields[ $key ]->valid_max;
						$title = $this->field_set->fields[ $key ]->title; 
						$component = new $class_name( $title, $name, $value, $min, $max  );
						$html .= '<td>' . $component->renderCell() . '</td>';
					}
					$i++;
				}
			$html .= '</tr>';		
		}
		$html .= '</tbody>';
		$html .= '</table>';
		return $html;
	}
	public function prepareRow( $row_id ){
		global $wpdb;
		$html = '';
		foreach ( $this->field_set->fields as $field ) { 
			if ( !$field->render_control ){
				continue;
			}		 
			$value = $wpdb->get_var( $wpdb->prepare( 
							"
								SELECT $field->name
								FROM  $this->table_name
								WHERE id = %d
							", 
						    $row_id
						) );
			$component = new $field->component( $field->title , $field->name, $value );
			$html .=  $component->renderControl();		 
		};
	 	$html .= '<div class="slf_control_container">';
		$html .= '<a href="#" class="slf_button slf_table_row_save">' . __( 'Save', 'wbk' ) . '</a>';	 
		$html .= '<a href="#" class="slf_button slf_table_row_cancel">' . __( 'Cancel', 'wbk' ) . '</a>';	 
		$html .= '</div>';
		echo $html;
	 }
	public function deleteRow(){
	}
}
?>