<?php
// Solo Framework table component control class
if ( ! defined( 'ABSPATH' ) ) exit;

class SLFTableComponent extends stdClass {
 
	
	public function __construct( $title, $name, $value, $min, $max  ) {

        $this->title = $title;      
        $this->name = $name;
        $this->value = $value;
        $this->min = $min;
        $this->max = $max;      
        
 	}
    public function renderCell(){


    }
    public function renderControl(){


    }


}
