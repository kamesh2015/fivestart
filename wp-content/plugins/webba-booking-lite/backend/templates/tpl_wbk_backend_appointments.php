<!-- Webba Booking backend options page template --> 
<?php
    // check if accessed directly
    if ( ! defined( 'ABSPATH' ) ) exit;
?>
<div class="wrap">
 	<h2 class="wbk_panel_title"><?php  echo __( 'Appointments', 'wbk' ); ?>
    <a style="text-decoration:none;" href="http://webba-booking.com/documentation/working-with-appointments" target="_blank"><span class="dashicons dashicons-editor-help"></span></a>
    </h2>
        <?php        
            $table = new WBK_Appointments_Table();
            $html = $table->render();
            echo $html;
        ?>                                            
</div>
