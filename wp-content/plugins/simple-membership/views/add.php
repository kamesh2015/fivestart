<div class="swpm-registration-widget-form">
    <form id="swpm-registration-form" name="swpm-registration-form" method="post" action="">
        <input type ="hidden" name="level_identifier" value="<?php echo $level_identifier ?>" />
        <table>
             <tr class="swpm-registration-firstname-row">
                <td><label for="first_name"><?php echo SwpmUtils::_('First Name') ?><span class="require">*</span></label></td>
                <td><input type="text" id="first_name" value="<?php echo $first_name; ?>" size="50" name="first_name" class="validate[required] alphabets_only" maxlength="20" /></td>
            </tr>
            <tr class="swpm-registration-lastname-row">
                <td><label for="last_name"><?php echo SwpmUtils::_('Last Name') ?><span class="require">*</span></label></td>
                <td><input type="text" id="last_name" value="<?php echo $last_name; ?>" size="50" name="last_name" class="validate[required] alphabets_only" maxlength="20"/></td>
            </tr>
            <tr class="swpm-registration-username-row">
		 <?php if($membership_level != '2'){  ?>
                <td><label for="user_name"><?php echo SwpmUtils::_('Business Name') ?><span class="require">*</span></label></td>
		<?php }else{?>
		<td><label for="user_name"><?php echo SwpmUtils::_('Username') ?><span class="require">*</span></label></td>
		<?php }?>
                <td><input type="text" id="user_name" class="validate[required,custom[noapostrophe],custom[SWPMUserName],minSize[4],ajax[ajaxUserCall]]" value="<?php echo $user_name; ?>" size="50" name="user_name" maxlength="20" /></td>
            </tr>
            <tr class="swpm-registration-email-row">
                <td><label for="email"><?php echo SwpmUtils::_('Email') ?><span class="require">*</span></label></td>
                <td><input type="text" id="email" class="validate[required,custom[email],ajax[ajaxEmailCall]]" value="<?php echo $email; ?>" size="50" name="email" maxlength="35"/></td>
            </tr>
            <tr class="swpm-registration-password-row">
                <td><label for="password"><?php echo SwpmUtils::_('Password') ?><span class="require">*</span></label></td>
                <td><input type="password" autocomplete="off" id="password" value="" size="50" name="password" class="validate[required]" maxlength="20"/></td>
            </tr>
            <tr class="swpm-registration-password-retype-row">
                <td><label for="password_re"><?php echo SwpmUtils::_('Confirm Password') ?><span class="require">*</span></label></td>
                <td><input type="password" autocomplete="off" id="password_re" value="" size="50" name="password_re" maxlength="20" class="validate[required]"/></td>
            </tr>

             
            <tr class="swpm-profile-zipcode-row">
                <td><label for="address_zipcode"><?php echo  SwpmUtils::_('Postal code')?><span class="require">*</span></label></td>
                <td><input type="text" id="address_zipcode" value="<?php echo  $address_zipcode; ?>" size="50" name="address_zipcode" maxlength="20" class="validate[required]"/></td>
            </tr>
           <tr class="swpm-profile-city-row">
            <td><label for="address_city"><?php echo  SwpmUtils::_('City')?><span class="require">*</span></label></td>
            <td><input type="text" id="address_city" value="<?php echo  $address_city; ?>" size="50" name="address_city" class="validate[required] alphabets_only" maxlength="20" /></td>
        </tr>
            <?php if($membership_level != '2'):  ?>
             <tr class="swpm-profile-phone-row">
            <td><label for="phone"><?php echo  SwpmUtils::_('Phone')?><span class="require">*</span></label></td>
            <td><input type="text" id="phone" value="<?php echo  $phone; ?>" size="50" name="phone" class="validate[required] number_only" maxlength="20"/></td>
        </tr>
           <tr class="swpm-profile-street-row">
		  <?php if($membership_level != '2'){  ?>
		<td><label for="address_street"><?php echo  SwpmUtils::_('Business Address')?></label></td>
		<?php }else{?>
            <td><label for="address_street"><?php echo  SwpmUtils::_('Address')?></label></td>
		<?php }?>
            <td><textarea id="address_street" name="address_street" maxlength="70"><?php echo  $address_street; ?></textarea></td>
        </tr>
            <?php endif;?>
         <tr class="swpm-term-row">
            <td><label for="term"><?php echo  SwpmUtils::_('Terms and Conditions')?><span class="require">*</span></label></td>
            <td><input type="checkbox" id="term" value="<?php echo  $term; ?>" size="50" name="term" class="validate[required]"/>
            <a href="<?php echo get_permalink(11121);?>" target="_blank">Please accept the terms and conditions</a>
            </td>
        </tr>
        <tr class="swpm-privacy-row">
            <td><label for="privacy"><?php echo  SwpmUtils::_('Privacy Policy')?><span class="require">*</span></label></td>
            <td><input type="checkbox" id="privacy" value="<?php echo  $privacy; ?>" size="50" name="privacy" class="validate[required]"/>
                <a href="<?php echo get_permalink(981);?>" target="_blank">Please accept the privacy policy</a>
            </td>
        </tr>
            <!-- <tr class="swpm-registration-membership-level-row">
                <td><label for="membership_level"><?php echo SwpmUtils::_('Membership Level') ?></label></td>
                <td>
                    <?php //echo $membership_level_alias; ?>
                    
                </td>
            </tr>            -->
            <input type="hidden" value="<?php echo $membership_level; ?>" size="50" name="membership_level" id="membership_level" />
        </table>        
        
        <div class="swpm-before-registration-submit-section" align="center"><?php echo apply_filters('swpm_before_registration_submit_button', ''); ?></div>
        
        <div class="swpm-registration-submit-section" align="center">
            <input type="submit" value="<?php echo SwpmUtils::_('Register') ?>" class="swpm-registration-submit" name="swpm_registration_submit" />
        </div>
        
        <input type="hidden" name="action" value="custom_posts" />
        
    </form>
</div>
<script>
    jQuery(document).ready(function ($) {
        $.validationEngineLanguage.allRules['ajaxUserCall']['url'] = '<?php echo admin_url('admin-ajax.php'); ?>';
        $.validationEngineLanguage.allRules['ajaxEmailCall']['url'] = '<?php echo admin_url('admin-ajax.php'); ?>';
        $.validationEngineLanguage.allRules['ajaxEmailCall']['extraData'] = '&action=swpm_validate_email&member_id=<?php echo filter_input(INPUT_GET, 'member_id'); ?>';
        $("#swpm-registration-form").validationEngine('attach');
    });
</script>

