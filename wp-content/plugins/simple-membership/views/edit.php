<?php $user_id = get_current_user_id( ); ?>
<?php $favorite_cut = get_user_meta( $user_id, 'favorite_cut', 'true' ); ?>
<?php $type_hair = get_user_meta( $user_id, 'type_hair', 'true' ); ?>
<?php $profile_img = get_user_meta( $user_id, 'avtar_image', 'true' );
$profile_dob = get_user_meta( $user_id, 'profile_dob', 'true' );
$booking_code = get_user_meta( $user_id, 'booking_code', 'true' );
//if(empty($profile_dob)){
  //  $profile_dob = '10-01-1980';
//}
$premises = get_user_meta( $user_id, 'premises', 'true' );
 ?>
<div class="swpm-edit-profile-form">
<form id="swpm-editprofile-form" name="swpm-editprofile-form" method="post" action="" enctype="multipart/form-data">
    <table>
        <tr class="swpm-profile-avtar_image-row">
            <td><label for="avtar_image"><?php echo  SwpmUtils::_('Profile Image') ?></label></td>
            <td><input type="file" id="avtar_image" name="avtar_image" value="<?php echo $profile_img;?>" />
           <?php if(!empty($profile_img)){?><img src="<?php echo $profile_img;?>" alt="<?php echo  SwpmUtils::_('Username') ?>" width="50px"/><?php }?></td>
        </tr>
        <tr class="swpm-profile-username-row">
            <td><label for="user_name"><?php echo  SwpmUtils::_('Username') ?></label></td>
            <td><?php echo  $user_name ?> (<?php echo  $membership_level_alias; ?>)</td>
        </tr>
        <tr class="swpm-profile-email-row">
            <td><label for="email"><?php echo  SwpmUtils::_('Email')?></label></td>
            <td><?php echo $email;?><input type="hidden" id="email" class="validate[required,custom[email],ajax[ajaxEmailCall]]" value="<?php echo $email;?>" size="50" name="email" maxlength="35"/></td>
        </tr>
        <tr class="swpm-profile-password-row">
            <td><label for="password"><?php echo  SwpmUtils::_('Change Password')?></label></td>
            <td><input type="password" id="password" value=""  size="50" name="password" maxlength="20"/></td>
        </tr>
        <tr class="swpm-profile-password-retype-row">
            <td><label for="password_re"><?php echo  SwpmUtils::_('Re-Enter Password')?></label></td>
            <td><input type="password" id="password_re" value="" size="50" name="password_re" maxlength="20"/></td>
        </tr>
	 <tr class="swpm-profile-firstname-row">
            <td><label for="first_name"><?php echo  SwpmUtils::_('First Name')?></label></td>
            <td><input type="text" id="first_name" value="<?php echo  $first_name; ?>" size="50" name="first_name" maxlength="20" class="alphabets_only"/></td>
        </tr>
        <tr class="swpm-profile-lastname-row">
            <td><label for="last_name"><?php echo  SwpmUtils::_('Last Name')?></label></td>
            <td><input type="text" id="last_name" value="<?php echo  $last_name; ?>" size="50" name="last_name" maxlength="20" class="alphabets_only"/></td>
        </tr>
	 <tr class="swpm-profile-phone-row">
            <td><label for="phone"><?php echo  SwpmUtils::_('Phone')?></label></td>
            <td><input type="text" id="phone" value="<?php echo  $phone; ?>" size="50" name="phone" maxlength="20" class="number_only"/></td>
        </tr>
	 <?php if($membership_level == '2'):  ?>
         <tr class="swpm-profile-dob-row">
            <td><label for="address_dob"><?php echo  SwpmUtils::_('Date of birth')?></label></td>
            <td><input type="text" id="address_dob" value="<?php echo  $profile_dob; ?>" size="50" name="profile_dob" placeholder="01-08-2016"/$
        </tr>
	<tr class="swpm-profile-city-row">
            <td><label for="address_city"><?php echo  SwpmUtils::_('City')?></label></td>
            <td><input type="text" id="address_city" value="<?php echo  $address_city; ?>" size="50" name="address_city" maxlength="20" class="alphabets_only"/></td>
        </tr>
	<?php endif;?>

	<?php if($membership_level == '2'):  ?>
	<tr class="swpm-profile-state-row">
            <td><label for="address_state"><?php echo  SwpmUtils::_('State')?></label></td>
            <td><input type="text" id="address_state" value="<?php echo  $address_state; ?>" size="50" name="address_state" class="alphabets_only" maxlength="20"/></td>
        </tr> 
        <tr class="swpm-profile-street-row">
            <td><label for="address_street"><?php echo  SwpmUtils::_('Address')?></label></td>
            <td>
            <textarea id="address_street" name="address_street"  maxlength="70"><?php echo  $address_street; ?></textarea>
                </td>
        </tr>

	<?php endif;?>
        <?php if($membership_level != '2'):  ?>
		 <tr class="swpm-profile-zipcode-row">
            <td><label for="address_zipcode"><?php echo  SwpmUtils::_('Postal code')?></label></td>
            <td><input type="text" id="address_zipcode" value="<?php echo  $address_zipcode; ?>" size="50" name="address_zipcode" /></td>
        </tr>

	<!--<tr class="swpm-premises-row">
            <td><label for="premises"><?php echo  SwpmUtils::_('Premises')?></label></td>
            <td><select id="premises" name="premises">
                <option value="mobile" <?php if($premises == 'mobile'){ echo 'selected="selected"';}?>>Mobile</option>
                <option value="shopbased" <?php if($premises == 'shopbased'){ echo 'selected="selected"';}?>>Shop based</option>
            </select>
            </td>
        </tr>-->
        <?php endif;?>            
          <?php if($membership_level == '2'):  ?>

        <tr class="swpm-favorite-cut-row">
            <td><label for="favorite-cut"><?php echo  SwpmUtils::_('Favorite cut')?></label></td>
            <td>
            <input type="text" id="favorite-cut" name="favorite-cut" value="<?php echo  $favorite_cut; ?>" class="alphabets_only" maxlength="35">
            </td>
        </tr>
        <?php //$taxonomies = get_taxonomies(); 
        //print_r($taxonomies);
//        $terms = get_terms( array('taxonomy' => 'job_listing_category','hide_empty' => false, ) );
        ?>
        <!--<tr class="swpm-type-of-hair-row">
            <td><label for="type-of-hair"><?php echo  SwpmUtils::_('Type of hair')?></label></td>
            <td>
            <select id="type-of-hair" name="type-of-hair">
            <?php foreach ($terms as $key => $value) {?>
                   <option value="<?php echo $value->term_id;?>" <?php if($type_hair == $value->term_id){ echo 'selected="selected"'; }?>><?php echo $value->name;?></option>
            <?php }?>
            <?php //echo  $type_of_hair; ?>
             </select>
            </td>
        </tr>-->
         <?php endif;?> 
 <tr class="swpm-profile-country-row">
            <td><label for="country"><?php echo  SwpmUtils::_('Nationality') ?></label></td>
            <td><input type="text" id="country" value="<?php echo  $country; ?>" size="50" name="country" class="alphabets_only" maxlength="30"/></td>
        </tr> 
        <?php if($membership_level != '2'):  ?>
        <tr class="swpm-profile-street-row">
            <td><label for="booking_code"><?php echo  SwpmUtils::_('Booking Embed code ')?></label></td>
            <td>
            <textarea id="booking_code" name="booking_code" ><?php echo $booking_code; ?></textarea>
                </td>
        </tr>
    <?php endif;?>
        <!-- <tr class="swpm-profile-membership-level-row">
            <td><label for="membership_level"><?php echo  SwpmUtils::_('Membership Level')?></label></td>
            <td>
                <?php echo  $membership_level_alias; ?>
            </td>
        </tr> -->
    </table>
    <p class="swpm-edit-profile-submit-section">
        <input type="submit" value="<?php echo SwpmUtils::_('Update')?>" class="swpm-edit-profile-submit" name="swpm_editprofile_submit" />
    </p>
    <?php echo SwpmUtils::delete_account_button(); ?>
    
    <input type="hidden" name="action" value="custom_posts" />

</form>
</div>
<script>
jQuery(document).ready(function($){
    $.validationEngineLanguage.allRules['ajaxEmailCall']['url']= '<?php echo admin_url('admin-ajax.php');?>';
    $.validationEngineLanguage.allRules['ajaxEmailCall']['extraData'] = '&action=swpm_validate_email&member_id=<?php echo SwpmAuth::get_instance()->get('member_id'); ?>';
    $("#swpm-editprofile-form").validationEngine('attach');
});
</script>
<?php
    wp_enqueue_script('jquery-ui-datepicker');
    wp_enqueue_style('jquery-style', 'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.2/themes/smoothness/jquery-ui.css');
?>
<script>
    jQuery(document).ready(function() {
        jQuery('#address_dob').datepicker({
            dateFormat : 'dd-mm-yy'
        });
    });
</script>
